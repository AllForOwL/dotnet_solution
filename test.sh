#!/bin/bash

set -e

dotnet restore ./dotnet_solution.sln
dotnet test ./tests/tests.csproj
