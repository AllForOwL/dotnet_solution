#!/bin/bash

set -e

dotnet restore ./dotnet_solution.sln
dotnet build ./src/src.csproj