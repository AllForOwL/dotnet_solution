FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# Copy everything else and build
COPY . ./

# Build runtime image
FROM microsoft/aspnetcore:2.0
# COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "src.dll"]